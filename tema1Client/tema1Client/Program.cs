﻿using System;
using tema1Service.Protos;
using Grpc.Net.Client;
using System.Threading.Tasks;

namespace tema1Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string read_cnp;
            string read_name;
            read_name = Console.ReadLine();
            read_cnp = Console.ReadLine();
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Client.ClientClient(channel);
            var response = await client.GetInfoAsync(new ClientRequest { MCNP = read_cnp, MName = read_name }) ;
            Console.WriteLine(response);
        }
    }
}
