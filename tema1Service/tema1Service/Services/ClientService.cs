﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tema1Service.Protos;
using tema1Service;
using Microsoft.Extensions.Logging;

namespace tema1Service.Services
{
    public class ClientService : Client.ClientBase
    {
        private readonly ILogger<ClientService> _logger;
        public ClientService(ILogger<ClientService> logger)
        {
            _logger = logger;
        }
        public override Task<ClientReply> GetInfo(ClientRequest clientRequest, ServerCallContext context)
        {
            _logger.Log(LogLevel.Information, clientRequest.Nume + " " + clientRequest.CNP);
            string m_cnp = clientRequest.CNP;
      
            DateTime date_today = DateTime.Today;
            string year_today = date_today.ToString("yyyy");
            string month_today = date_today.ToString("MM");
            string day_today = date_today.ToString("dd");

            string cnp_year;
            if (m_cnp[0] == '0' || m_cnp[0] == '1')
            { cnp_year = "19" + m_cnp[1].ToString() + m_cnp[2].ToString(); }
            else
            { cnp_year = "20" + m_cnp[1].ToString() + m_cnp[2].ToString(); }
            string cnp_month = m_cnp[3].ToString() + m_cnp[4].ToString();
            string cnp_day = m_cnp[5].ToString() + m_cnp[6].ToString();

            

            return Task.FromResult(new ClientReply { Reply = "1" });
        }
    }
}
